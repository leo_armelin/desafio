export const filterColors = [
  {
    color: "#000000",
  },
  {
    color: "#F36324",
  },
  {
    color: "#fcbf49",
  },
  {
    color: "#ffa69e",
  },
  {
    color: "#ccc5b9",
  },
  {
    color: "#28A3AA",
  },
  {
    color: "#edb183",
  },
];

export const getColorName = (color: string) => {
  if (color === "#000000") return "Preta";
  if (color === "#F36324") return "Laranja";
  if (color === "#fcbf49") return "Amarela";
  if (color === "#ffa69e") return "Rosa";
  if (color === "#ccc5b9") return "Cinza";
  if (color === "#28A3AA") return "Azul";
  return "Bege";
};
