import React from "react";
import { ThemeProvider } from "styled-components";

const theme = {
  colors: {
    primary: "#FFFFFF",
    secondary: "#CC0D1F",
    blue: "#80BDB8",
    textBody: "#A0A0A0",
  },
  constants: {
    MAX_WIDTH: "64rem",
  },
};

const Theme = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default Theme;
