import Router, { useRouter } from "next/router";
import Link from "next/link";
import { useState } from "react";
import {
  Main,
  Content,
  Title,
  RedText,
  ShopContainer,
  Filter,
  FilterTitle,
  Section,
  FilterSubtitle,
  GenderContainer,
  GenderItem,
  ColorContainer,
  ColorItem,
  Products,
  ProductsTitle,
  ProductsOrdenation,
  IconButton,
  GridIcon,
  ListIcon,
  OrdenationText,
  Selector,
  SelectorOption,
  ProductsGrid,
  ItemContainer,
  ItemImgContainer,
  ItemImg,
  ItemName,
  ItemPriceContainer,
  ItemDiscount,
  ItemPrice,
  ItemButton,
  PaginationContainer,
  CategoryItem,
} from "../../../styles/shop";

import api from "../../services/api";
import { filterColors, getColorName } from "../../configs/filterColors";
import numberToMoney from "../../utils/numberToMoney";

import { Pagination } from "../../components/Pagination";
import { SEO } from "../../components/SEO";
import FilterLoader from "../../loaders/FilterLoader";

import {
  ItemsProps,
  ShopResponse,
  FilterProps,
  CategoriesProps,
} from "../../interfaces/shop";

export default function Shop({
  filters,
  items,
  categories,
  error,
}: ShopResponse) {
  const router = useRouter();
  const queries = router.query as FilterProps;

  const [isGrid, setIsGrid] = useState(true);
  const [page, setPage] = useState(1);
  const [totalPages] = useState(10);

  const pushQuery = (pathname: string, query: { [key: string]: string }) => {
    Router.push({ pathname, query });
  };

  const getRouteName = () => {
    const pathname = queries.product;
    if (pathname === "camisetas") return "Camisetas";
    if (pathname === "calcas") return "Calças";
    return "Calçados";
  };

  const GenderSection = () => (
    <GenderContainer>
      <GenderItem
        onClick={() =>
          pushQuery(router.asPath.split("?")[0], { gender: "Masculina" })
        }
      >
        Masculino
      </GenderItem>
      <GenderItem
        onClick={() =>
          pushQuery(router.asPath.split("?")[0], { gender: "Feminina" })
        }
      >
        Feminino
      </GenderItem>
    </GenderContainer>
  );

  const ColorSection = () => (
    <ColorContainer>
      {filterColors.map((item) => (
        <ColorItem
          key={item.color}
          bgColor={item.color}
          onClick={() =>
            pushQuery(router.asPath.split("?")[0], {
              color: getColorName(item.color),
            })
          }
        />
      ))}
    </ColorContainer>
  );

  const GridItem = ({ item }: { item: ItemsProps }) => (
    <ItemContainer>
      <ItemImgContainer>
        <ItemImg src={item.image} layout="fill" objectFit="contain" />
      </ItemImgContainer>
      <ItemName>{item.name}</ItemName>
      <ItemPriceContainer>
        {item.specialPrice ? (
          <>
            <ItemDiscount>{numberToMoney(item.price)}</ItemDiscount>
            <ItemPrice>{numberToMoney(item.specialPrice)}</ItemPrice>
          </>
        ) : (
          <ItemPrice>{numberToMoney(item.price)}</ItemPrice>
        )}
      </ItemPriceContainer>
      <ItemButton>COMPRAR</ItemButton>
    </ItemContainer>
  );

  return (
    <Main>
      <SEO title={getRouteName()} />

      <Content>
        <Title>
          Página inicial &gt; <RedText>{getRouteName()}</RedText>
        </Title>

        <ShopContainer>
          {!filters || !categories ? (
            <FilterLoader />
          ) : (
            <Filter>
              <FilterTitle>FILTRE POR</FilterTitle>

              <Section>
                <FilterSubtitle>Categorias</FilterSubtitle>
                {categories.map((item: CategoriesProps) => (
                  <Link href={`/shop/${item.path}`} key={item.id} passHref>
                    <CategoryItem>{item.name}</CategoryItem>
                  </Link>
                ))}
              </Section>

              {Object.keys(filters).map((section) => (
                <Section key={section}>
                  <FilterSubtitle>{filters[section]}</FilterSubtitle>
                  {section === "gender" && <GenderSection />}
                  {section === "color" && <ColorSection />}
                </Section>
              ))}
            </Filter>
          )}

          {items && (
            <Products>
              <ProductsTitle>{getRouteName()}</ProductsTitle>

              <ProductsOrdenation>
                <IconButton onClick={() => setIsGrid(true)}>
                  <GridIcon selected={isGrid} />
                </IconButton>
                <IconButton onClick={() => setIsGrid(false)}>
                  <ListIcon selected={!isGrid} />
                </IconButton>

                <OrdenationText>Ordenar por:</OrdenationText>
                <Selector>
                  <SelectorOption>Preço</SelectorOption>
                </Selector>
              </ProductsOrdenation>

              <ProductsGrid>
                {!queries.gender &&
                  items.map(
                    (item) =>
                      item.filter[0].color === queries.color && (
                        <GridItem item={item} key={item.id} />
                      )
                  )}
                {!queries.color &&
                  items.map(
                    (item) =>
                      item.filter[0].gender === queries.gender && (
                        <GridItem item={item} key={item.id} />
                      )
                  )}
              </ProductsGrid>

              <PaginationContainer>
                <Pagination
                  actuallyPage={page}
                  setPage={setPage}
                  totalPages={totalPages}
                />
              </PaginationContainer>
            </Products>
          )}
        </ShopContainer>
      </Content>
    </Main>
  );
}

export async function getStaticProps({ params }) {
  try {
    const { product } = params;

    const convertPath = {
      camisetas: 1,
      calcas: 2,
      calcados: 3,
    };

    const requests = [api.get(`${convertPath[product]}`), api.get("list")];

    const [{ data: mainData }, { data: menuData }] = await Promise.all(
      requests
    );

    return {
      props: {
        filters: mainData.filters[0],
        items: mainData.items,
        categories: menuData.items,
      },
      revalidate: 300,
    };
  } catch (error) {
    return { props: { error: error.message } };
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}
