import ThemeProvider from "../providers/ThemeProvider";
import GlobalStyles from "../../styles/global";
import { Footer } from "../../styles/Home";
import { Header } from "../components/Header";
import { AuthNav } from "../components/AuthNav";
import { Menu } from "../components/Menu";
import { useState } from "react";

function MyApp({ Component, pageProps }) {
  const [isActive, setIsActive] = useState(false);

  const openMenu = () => {
    setIsActive(true);
    document.body.style.overflow = "hidden";
    document.body.style.position = "fixed";
  };

  const closeMenu = () => {
    setIsActive(false);
    document.body.style.overflowY = "unset";
    document.body.style.position = "initial";
  };

  return (
    <ThemeProvider>
      <AuthNav />
      <Header openMenu={openMenu} />
      <Menu closeMenu={closeMenu} isActive={isActive} />
      <Component {...pageProps} />
      <Footer />
      <GlobalStyles />
    </ThemeProvider>
  );
}

export default MyApp;
