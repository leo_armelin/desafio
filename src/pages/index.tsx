import Head from "next/head";
import {
  Main,
  Content,
  ListPagesContainer,
  ListPages,
  ListOption,
  HeroContainer,
  DescriptionContainer,
  DescriptionTitle,
  DescriptionText,
} from "../../styles/Home";
import { SEO } from "../components/SEO";

import { loremIpsum } from "../mock/loremIpsum";

export default function Home() {
  return (
    <div>
      <SEO title="Bem-vindo à Webjump!" excludeTitleSuffix />

      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>

      <Main>
        <Content>
          <ListPagesContainer>
            <ListPages>
              <ListOption>Página Inicial</ListOption>
              <ListOption>Camisetas</ListOption>
              <ListOption>Calças</ListOption>
              <ListOption>Sapatos</ListOption>
              <ListOption>Contato</ListOption>
            </ListPages>
          </ListPagesContainer>

          <HeroContainer />

          <DescriptionContainer>
            <DescriptionTitle>Seja bem-vindo!</DescriptionTitle>
            <DescriptionText>{loremIpsum}</DescriptionText>
          </DescriptionContainer>
        </Content>
      </Main>
    </div>
  );
}
