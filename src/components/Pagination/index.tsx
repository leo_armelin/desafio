import pagination from "../../utils/pagination";
import {
  Container,
  PageButton,
  ArowLeftIcon,
  ArowRightIcon,
  Dots,
} from "./styles";

interface PaginationProps {
  actuallyPage: number;
  totalPages: number;
  setPage: (page: number) => void;
}

export function Pagination({
  actuallyPage,
  totalPages,
  setPage,
}: PaginationProps) {
  const pages = pagination(actuallyPage, totalPages);

  const nextPage = () => {
    setPage(actuallyPage + 1);
  };

  const prevPage = () => {
    setPage(actuallyPage - 1);
  };

  const goToPage = (page) => {
    setPage(page);
  };

  return (
    <Container>
      <PageButton
        type="button"
        onClick={prevPage}
        disabled={actuallyPage === 1}
      >
        <ArowLeftIcon />
      </PageButton>
      {pages.map((page) =>
        page ? (
          <PageButton
            key={page}
            type="button"
            onClick={() => goToPage(page)}
            disabled={actuallyPage === page}
            actual={actuallyPage === page}
          >
            {page}
          </PageButton>
        ) : (
          <Dots key={Math.random()}>...</Dots>
        )
      )}
      <PageButton
        type="button"
        onClick={nextPage}
        disabled={actuallyPage === totalPages}
      >
        <ArowRightIcon />
      </PageButton>
    </Container>
  );
}
