import styled from "styled-components";
import { FiChevronLeft, FiChevronRight } from "react-icons/fi";

interface PageProps {
  actual?: boolean;
}

export const Container = styled.div`
  display: flex;

  justify-content: center;
  align-items: center;

  width: 100%;

  gap: 0.25rem;
`;

export const PageButton = styled.button<PageProps>`
  display: flex;

  align-items: center;

  border: solid 0.1rem var(--grey200);
  border-radius: 10rem;
  padding: 0.5rem 1rem;

  font-size: 0.85rem;
  font-weight: ${(props) => (props.actual ? 800 : 400)};
  text-transform: uppercase;

  background-color: transparent;
  color: ${(props) =>
    props.actual ? props.theme.colors.secondary : props.theme.colors.textBody};

  transition: background-color 250ms, color 250ms;

  &:hover {
    color: ${({ theme }) => theme.colors.secondary};
    font-weight: 800;
  }

  &:disabled {
    cursor: initial;
  }

  svg {
    height: 1.2rem;
    width: 1.2rem;
    color: #80bdb8;
  }

  @media (max-width: 767px) {
    padding: 0.25rem 0.5rem;
  }
`;

export const ArowLeftIcon = styled(FiChevronLeft)``;

export const ArowRightIcon = styled(FiChevronRight)``;

export const Dots = styled.p`
  color: ${({ theme }) => theme.colors.textBody};
`;
