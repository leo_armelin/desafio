import { useState } from "react";
import { SearchBarContainer, Input, Button } from "./styles";

export function SearchBar() {
  const [searchText, setSearchText] = useState("");

  return (
    <SearchBarContainer>
      <Input
        onChange={(e) => setSearchText(e.target.value)}
        value={searchText}
      />

      <Button>BUSCAR</Button>
    </SearchBarContainer>
  );
}
