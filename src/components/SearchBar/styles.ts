import styled from "styled-components";

export const SearchBarContainer = styled.div`
  display: flex;
  flex-direction: row;

  height: 3rem;
  width: 30rem;

  @media (max-width: 767px) {
    display: none;
  }
`;

export const Input = styled.input`
  height: 100%;
  flex-grow: 1;

  border: 0.125em solid ${({ theme }) => theme.colors.textBody};
  padding: 0 0.875rem;
`;

export const Button = styled.button`
  display: flex;

  align-items: center;

  height: 3rem;
  width: fit-content;

  border: none;
  padding: 0 1.75rem;

  font-weight: 800;

  background-color: ${({ theme }) => theme.colors.secondary};
  color: #fff;
`;
