import Link from "next/link";

import {
  HeaderContainer,
  HeaderContent,
  LogoContainer,
  Logo,
  MenuIcon,
  SearchIcon,
} from "./styles";
import { SearchBar } from "../SearchBar";

interface HeaderProps {
  openMenu: () => void;
}
export function Header({ openMenu = () => null }: HeaderProps) {
  return (
    <HeaderContainer>
      <HeaderContent>
        <MenuIcon onClick={openMenu} />

        <Link href="/" passHref>
          <LogoContainer>
            <Logo src="/assets/images/logo-webjump.png" />
          </LogoContainer>
        </Link>

        <SearchBar />
        <SearchIcon />
      </HeaderContent>
    </HeaderContainer>
  );
}
