import styled from "styled-components";
import { BiMenu, BiSearch } from "react-icons/bi";

export const HeaderContainer = styled.header`
  display: flex;
  flex-direction: row;

  align-items: center;
  justify-content: center;

  height: fit-content;
  width: 100vw;

  padding: 2rem 0;

  background-color: ${({ theme }) => theme.colors.primary};
`;

export const HeaderContent = styled.div`
  display: flex;
  flex-direction: row;

  align-items: center;
  justify-content: space-between;

  max-width: ${({ theme }) => theme.constants.MAX_WIDTH};
  width: 95%;
`;

export const LogoContainer = styled.a`
  height: fit-content;
  width: 15rem;

  @media (max-width: 767px) {
    width: 12rem;
  }
`;

export const Logo = styled.img`
  height: auto;
  width: 100%;

  object-fit: contain;
`;

export const MenuIcon = styled(BiMenu)`
  display: none;

  height: 3.5rem;
  width: 3.5rem;

  padding: 0.5rem;

  @media (max-width: 767px) {
    display: block;
  }
`;

export const SearchIcon = styled(BiSearch)`
  display: none;

  height: 3.5rem;
  width: 3.5rem;

  padding: 0.5rem;

  color: ${({ theme }) => theme.colors.secondary};

  @media (max-width: 767px) {
    display: block;
  }
`;
