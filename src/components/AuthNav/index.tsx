import { AuthNavContainer, AuthNavContent, TextLink, Text } from "./styles";

export function AuthNav() {
  return (
    <AuthNavContainer>
      <AuthNavContent>
        <Text>
          <TextLink>Acesse sua Conta</TextLink> ou{" "}
          <TextLink>Cadastre-se</TextLink>
        </Text>
      </AuthNavContent>
    </AuthNavContainer>
  );
}
