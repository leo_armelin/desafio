import styled from "styled-components";

export const AuthNavContainer = styled.nav`
  display: flex;
  flex-direction: row;

  justify-content: center;

  height: fit-content;
  width: 100vw;

  padding: 0.5rem 0;

  background-color: black;
`;

export const AuthNavContent = styled.div`
  display: flex;
  flex-direction: row;

  justify-content: flex-end;

  max-width: ${({ theme }) => theme.constants.MAX_WIDTH};
  height: 100%;
  width: 95%;

  @media (max-width: 767px) {
    justify-content: center;
  }
`;

export const Text = styled.p`
  font-size: 1rem;
  color: #fff;
`;

export const TextLink = styled.a`
  font-weight: bold;
  text-decoration: underline;

  &:hover {
    cursor: pointer;
  }
`;
