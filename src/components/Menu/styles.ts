import styled, { keyframes } from "styled-components";
import { BiX } from "react-icons/bi";

interface IMenuContainer {
  isActive: boolean;
}

const hideMenu = keyframes`
  0% {opacity: 0}
  99% {opacity: 0}
  100% {opacity: 1}
`;

export const MenuContainer = styled.nav<IMenuContainer>`
  display: flex;
  flex-direction: row;

  justify-content: center;

  height: fit-content;
  width: 100vw;

  background-color: ${({ theme }) => theme.colors.secondary};

  @media (max-width: 767px) {
    position: fixed;
    top: 0;
    left: 0;

    height: 100vh;

    z-index: 1000;

    animation: 1s ${hideMenu} ease;
    transform: translateY(${(props) => (props.isActive ? "0%" : "-100%")});
    transition: transform 500ms ease;
  }
`;

export const MenuContent = styled.div`
  display: flex;
  flex-direction: row;

  justify-content: flex-start;

  max-width: ${({ theme }) => theme.constants.MAX_WIDTH};
  height: 100%;
  width: 95%;

  transform: translateX(-2rem);

  @media (max-width: 1023px) {
    justify-content: space-evenly;

    transform: translateX(-0.5rem);
  }

  @media (max-width: 767px) {
    flex-direction: column;

    align-items: center;
    justify-content: center;
  }
`;

export const MenuOptionLink = styled.a`
  width: fit-content;

  padding: 1rem 2rem;

  font-size: 1.25rem;
  font-weight: 800;
  text-decoration: none;

  background-color: ${({ theme }) => theme.colors.secondary};
  color: #fff;

  &:hover {
    cursor: pointer;
    filter: brightness(0.9);
  }

  @media (max-width: 1023px) {
    padding: 1rem 0.5rem;
  }

  @media (max-width: 767px) {
    width: 100%;

    text-align: center;
  }
`;

export const MenuIcon = styled(BiX)`
  display: none;

  height: 3.5rem;
  width: 3.5rem;

  padding: 0.5rem;

  background-color: ${({ theme }) => theme.colors.secondary};
  color: #fff;

  &:hover {
    filter: brightness(0.9);
  }

  @media (max-width: 767px) {
    display: block;

    width: 100%;
  }
`;
