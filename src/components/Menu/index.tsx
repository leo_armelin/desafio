import Link from "next/link";
import PropTypes from "prop-types";
import { MenuContainer, MenuContent, MenuOptionLink, MenuIcon } from "./styles";

interface MenuOptionProps {
  href: string;
  children: React.ReactNode;
  onClick?: () => void;
}

export function Menu({ closeMenu, isActive }) {
  const MenuOption = ({ href, children, onClick }: MenuOptionProps) => (
    <Link href={href} passHref>
      <MenuOptionLink onClick={onClick}>{children}</MenuOptionLink>
    </Link>
  );

  return (
    <MenuContainer isActive={isActive}>
      <MenuContent>
        <MenuIcon onClick={closeMenu} />
        <MenuOption href="/" onClick={closeMenu}>
          PÁGINA INICIAL
        </MenuOption>
        <MenuOption href="/shop/camisetas" onClick={closeMenu}>
          CAMISETAS
        </MenuOption>
        <MenuOption href="/shop/calcas" onClick={closeMenu}>
          CALÇAS
        </MenuOption>
        <MenuOption href="/shop/calcados" onClick={closeMenu}>
          SAPATOS
        </MenuOption>
        <MenuOption href="/" onClick={closeMenu}>
          CONTATO
        </MenuOption>
      </MenuContent>
    </MenuContainer>
  );
}

Menu.propTypes = {
  closeMenu: PropTypes.func,
  isActive: PropTypes.bool,
};

Menu.defaultProps = {
  closeMenu: () => null,
  isActive: false,
};
