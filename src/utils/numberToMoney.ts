export default function numberToMoney(
  number: number,
  locale = "pt-br",
  currency = "BRL"
) {
  return number.toLocaleString(locale, { style: "currency", currency });
}
