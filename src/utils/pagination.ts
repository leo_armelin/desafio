export default function pagination(actuallyPage: number, totalPages: number) {
  const current = actuallyPage;
  const last = totalPages;
  const delta = 2;
  const left = current - delta + 1;
  const right = current + delta + 1;
  const range: number[] = [];
  const rangeWithDots: number[] = [];

  for (let index = 1; index <= last; index += 1) {
    if (index === 1 || index === last || (index >= left && index < right)) {
      range.push(index);
    }
  }

  let l;
  for (const i of range) {
    if (l) {
      if (i - l === 2) {
        rangeWithDots.push(l + 1);
      } else if (i - l !== 1) {
        rangeWithDots.push(null);
      }
    }
    rangeWithDots.push(i);
    l = i;
  }

  return rangeWithDots;
}
