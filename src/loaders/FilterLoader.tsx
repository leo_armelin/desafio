import styled, { keyframes } from "styled-components";
import { Filter, FilterTitle, Section } from "../../styles/shop";

const gradient = keyframes`
  0% {
      background-position: 0% 50%;
    }
    50% {
      background-position: 100% 50%;
    }
    100% {
      background-position: 0% 50%;
    }
`;

export const FilterSubtitle = styled.div`
  height: 1.25rem;
  width: 70%;

  border-radius: 1rem;

  background: linear-gradient(
    45deg,
    rgba(128, 189, 184, 1) 0%,
    rgba(231, 231, 231, 1) 100%
  );
  background-size: 200% 200%;
  animation: ${gradient} 15s ease infinite;
`;

export const CategoryItem = styled.div`
  height: 0.75rem;
  width: 40%;

  border-radius: 1rem;
  margin-top: 1rem;

  background: linear-gradient(
    45deg,
    rgba(131, 131, 131, 1) 0%,
    rgba(231, 231, 231, 1) 100%
  );
  background-size: 200% 200%;
  animation: ${gradient} 15s ease infinite;
`;

export default function FilterLoader() {
  return (
    <Filter>
      <FilterTitle>FILTRE POR</FilterTitle>

      <Section>
        <FilterSubtitle />
        <CategoryItem />
        <CategoryItem />
        <CategoryItem />
      </Section>
    </Filter>
  );
}
