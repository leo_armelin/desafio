export interface FilterProps {
  color?: string;
  gender?: string;
  product?: string;
}

export interface ItemsProps {
  key?: number;
  id: number;
  sku: string;
  path: string;
  name: string;
  image: string;
  price: number;
  specialPrice?: number;
  filter: FilterProps;
}

export interface CategoriesProps {
  id: number;
  name: string;
  path: string;
}

export interface ErrorProps {
  message: string;
}

export interface ShopResponse {
  filters: FilterProps;
  items: ItemsProps[];
  categories: CategoriesProps[];
  error?: ErrorProps;
}
