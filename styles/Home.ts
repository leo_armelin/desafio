import styled from "styled-components";

export const Main = styled.main`
  display: flex;

  justify-content: center;

  width: 100%;

  padding-top: 2rem;
`;

export const Content = styled.div`
  display: grid;
  grid-template-areas:
    "a b b"
    "a c c";
  grid-template-columns: 0.75fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-gap: 1.25rem;

  max-width: ${({ theme }) => theme.constants.MAX_WIDTH};
  width: 95%;

  @media (max-width: 1023px) {
    grid-template-columns: 0.75fr 1fr;
    grid-template-rows: 1fr 1.5fr;

    width: 90%;
  }

  @media (max-width: 767px) {
    grid-template-areas:
      "b"
      "c"
      "d";
    grid-template-columns: 1fr;
    grid-template-rows: auto 1fr;
  }
`;

export const ListPagesContainer = styled.section`
  grid-area: a;

  height: 36rem;
  width: 100%;

  padding: 2rem 1.5rem;

  background-color: #e2dedb;

  @media (max-width: 1023px) {
    height: 75%;

    padding: 1.5rem 1rem;
  }

  @media (max-width: 767px) {
    display: none;
  }
`;

export const ListPages = styled.ul`
  list-style-type: disc;

  padding-left: 1.5rem;

  li + li {
    margin-top: 1rem;
  }
`;

export const ListOption = styled.li`
  font-size: 1.25rem;
`;

export const HeroContainer = styled.section`
  grid-area: b;

  min-height: 8rem;
  height: 100%;
  width: 100%;

  background-color: #acacac;
`;

export const DescriptionContainer = styled.section`
  grid-area: c;

  height: 100%;
  width: 100%;
`;

export const DescriptionTitle = styled.h1`
  font-weight: 400;

  @media (max-width: 767px) {
    font-size: 2.5rem;
  }
`;

export const DescriptionText = styled.p`
  font-size: 1.25rem;
`;

export const Footer = styled.section`
  max-width: ${({ theme }) => theme.constants.MAX_WIDTH};
  height: 10rem;
  width: 95%;

  margin: 1.5rem auto;

  background-color: ${({ theme }) => theme.colors.secondary};

  @media (max-width: 767px) {
    height: 12rem;

    margin-top: 1rem;
  }
`;
