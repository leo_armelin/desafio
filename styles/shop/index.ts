import Image from "next/image";
import styled from "styled-components";
import { FaTh, FaThList } from "react-icons/fa";

interface ColorItemProps {
  bgColor: string;
}

interface IconProps {
  selected: boolean;
}

export const Main = styled.main`
  display: flex;

  justify-content: center;

  width: 100%;

  padding: 2rem 0;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;

  max-width: ${({ theme }) => theme.constants.MAX_WIDTH};
  width: 95%;

  @media (max-width: 1023px) {
    width: 90%;
  }
`;

export const Title = styled.h4`
  font-weight: 400;

  margin-bottom: 1rem;
`;

export const RedText = styled.span`
  color: ${({ theme }) => theme.colors.secondary};
`;

export const ShopContainer = styled.div`
  display: flex;

  width: 100%;

  gap: 1.5rem;

  @media (max-width: 1023px) {
    gap: 0.5rem;
  }

  @media (max-width: 767px) {
    flex-direction: column;
  }
`;

export const Filter = styled.section`
  height: fit-content;
  width: 18rem;

  border: 1px solid #ddd;
  padding: 1.25rem 1rem;

  @media (max-width: 1023px) {
    width: 13rem;
  }

  @media (max-width: 767px) {
    width: 100%;
  }
`;

export const FilterTitle = styled.h2`
  font-weight: 800;

  color: ${({ theme }) => theme.colors.secondary};
`;

export const Section = styled.section`
  margin-top: 1rem;
`;

export const FilterSubtitle = styled.h3`
  font-weight: 800;
  text-transform: uppercase;

  color: #80bdb8;
`;

export const GenderContainer = styled.ul`
  display: flex;
  flex-direction: column;

  padding: 0.5rem 1.25rem;

  list-style-type: disc;
`;

export const GenderItem = styled.li`
  padding: 0.125rem 0;

  color: ${({ theme }) => theme.colors.textBody};

  &:hover {
    cursor: pointer;
    font-weight: bold;
  }
`;

export const CategoryItem = styled(GenderItem)``;

export const ColorContainer = styled.ul`
  display: flex;

  height: 1.5rem;
  width: 70%;

  gap: 0.25rem;
  margin-top: 0.875rem;

  list-style-type: none;

  @media (max-width: 1023px) {
    width: 100%;
  }
`;

export const ColorItem = styled.li<ColorItemProps>`
  height: 100%;
  flex: 1;

  background-color: ${(props) => props.bgColor};

  transition: all 300ms ease;

  &:hover {
    cursor: pointer;
    flex: 1.5;
  }
`;

export const Products = styled.section`
  display: flex;
  flex-direction: column;

  flex: 1;
  height: fit-content;
`;

export const ProductsTitle = styled.h2`
  font-weight: 400;

  color: ${({ theme }) => theme.colors.secondary};
`;

export const ProductsOrdenation = styled.div`
  display: flex;

  align-items: center;

  width: 100%;

  border-top: 1px solid #dddd;
  border-bottom: 1px solid #dddd;
  margin: 0.75rem 0 1.25rem;
  padding: 0.25rem 0;

  @media (max-width: 767px) {
    border: none;
  }
`;

export const IconButton = styled.button`
  border: none;
  padding: 0.5rem 0.5rem 0.25rem;

  background-color: transparent;

  &:hover {
    svg {
      color: #80bdb8;
    }
  }

  @media (max-width: 767px) {
    display: none;
  }
`;

export const GridIcon = styled(FaTh)<IconProps>`
  height: 1rem;
  width: 1rem;

  color: ${(props) => (props.selected ? "#80bdb8" : "#8b8c8f")};
`;

export const ListIcon = styled(FaThList)<IconProps>`
  height: 1rem;
  width: 1rem;

  color: ${(props) => (props.selected ? "#80bdb8" : "#8b8c8f")};
`;

export const OrdenationText = styled.p`
  display: flex;

  align-items: center;

  margin: 0 1rem 0 auto;

  font-weight: 800;

  color: ${({ theme }) => theme.colors.textBody};
`;

export const Selector = styled.select`
  height: 1.5rem;
  width: 16rem;

  border-color: #ddd;

  @media (max-width: 767px) {
    max-width: 16rem;
    flex: 1;
  }
`;

export const SelectorOption = styled.option``;

export const ProductsGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(4, minmax(0, 1fr));
  grid-gap: 1rem;

  width: 100%;

  border: 1px solid #ddd;
  padding: 1rem;

  @media (max-width: 1023px) {
    grid-template-columns: repeat(3, minmax(0, 1fr));
  }

  @media (max-width: 767px) {
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }
`;

export const ItemContainer = styled.div`
  display: flex;
  flex-direction: column;

  align-self: center;
  justify-self: center;
  align-items: center;

  height: 18rem;
  width: 10rem;

  @media (max-width: 767px) {
    height: 16rem;
    width: 8rem;
  }
`;

export const ItemImgContainer = styled.div`
  position: relative;

  height: 10rem;
  width: 10rem;

  border: 1px solid #ddd;

  @media (max-width: 767px) {
    height: 8rem;
    width: 8rem;
  }
`;

export const ItemImg = styled(Image)``;

export const ItemName = styled.p`
  display: block;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  width: 100%;

  margin: 0.5rem 0 auto;
  padding: 0 0.5rem;

  text-align: center;
  text-transform: uppercase;
  -webkit-line-clamp: 2;
  text-overflow: ellipsis;

  overflow: hidden;

  color: ${({ theme }) => theme.colors.textBody};
`;

export const ItemPriceContainer = styled.div`
  display: flex;

  align-items: center;
  justify-content: center;

  gap: 0.5rem;
  margin-bottom: 0.5rem;

  @media (max-width: 1023px) {
    flex-direction: column;

    gap: 0;
  }
`;

export const ItemDiscount = styled.p`
  font-size: 0.875rem;

  text-decoration: line-through;

  color: ${({ theme }) => theme.colors.textBody};
`;

export const ItemPrice = styled.strong`
  font-weight: 800;

  color: ${({ theme }) => theme.colors.secondary};
`;

export const ItemButton = styled.button`
  width: 100%;

  border: none;
  padding: 0.25rem 0;

  font-size: 0.875rem;
  font-weight: 800;

  background-color: #80bdb8;
  color: #fff;

  &:hover {
    filter: brightness(0.95);
  }
`;

export const PaginationContainer = styled.nav`
  margin-top: 0.5rem;
`;
