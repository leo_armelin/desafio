import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    colors: {
      primary: string;
      secondary: string;
      blue: string;
      textBody: string;
    };
    constants: {
      MAX_WIDTH: string;
    };
  }
}
