Projeto feito em [Next.js](https://nextjs.org/) utilizando [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Inicialização

Instale os pacotes:

```bash
yarn install
```

Inicialmente, inicie o servidor local:

```bash
yarn server
```

Depois, inicie o servidor de desenvolvimento

```bash
npm run dev
# ou
yarn dev
```

Abra [http://localhost:8888](http://localhost:8888) com o seu navegador para visualizar o projeto.

## Tecnologias utilizadas

- **Next.js:** devido ao SSR e a facilidade de trabalhar com SEO, é uma alternativa interessante quando se trata de e-commerce.
- **Styled components:** ao unir essa biblioteca com a tipagem do Typescript, podemos facilmente seguir o desenvolvimento voltado à componentes (CDD), facilitando a manutenção do website e a implementação de futuras features.
- **Json server:** facilita o uso de bancos de dados que ainda não estão em nuvem, apesar das limitações.
- **Axios:** escolhi por achar seu uso simples, além de ter familiaridade com trabalhos passados.
- **React icons:** diversas opções de ícones, e alta compatibilidade com styled components.
- **Eslint/prettier:** Embora não tenha tirado um tempo para configurá-lo adequadamente, é essencial para manter o padrão de código e boas práticas.

## Soluções e comentários

#### Roteamento

Para todo o roteamento da aplicação, a API do Next.js foi utilizada. O uso de arquivos e/ou pastas com colchetes é útil quando se trata de rotas que podem mudar frequentemente, que é o caso de categorias de produtos de e-commerce.

#### Tempo de resposta

A fim de simular o tempo de resposta em requisições para o servidor local, foi colocado um delay de 750ms na inicialização do `json-server`. O delay está presente no script abaixo:

```bash
"server": "json-server server.json -w -d 750 -p 3333"
```

Assim, é possível verificar a existência dos loaders, garantindo informação na tela mesmo sem os dados da API.

#### Filtragem dos produtos

Dentro do arquivo `[product].tsx` é possível verificar a filtragem de cor ou gênero sobre os produtos da loja. É evidente que a filtragem não deve ocorrer no frontend, dada a redução de performance, e o produto final pior do que seria caso os itens já fossem retornados corretamente pelo backend.

Portanto, a filtragem apresentada é apenas para mostrar que a mesma é possível, mas não deve ser aplicada dessa forma.

#### Responsividade

Atendo-se aos breakpoints, utilizei os conceitos de flexbox e grid para lidar com grande parte da responsividade.

#### Paginação

Como a quantidade de produtos é reduzida, e não foi pedido, não criei um sistema funcional de paginação. Felizmente, tive a oportunidade de trabalhar com o mesmo em outro projeto, e uma possível solução seria a paginação através de SWR e queries.

## Agradecimentos

Obrigado pela oportunidade, fico no aguardo para entrarmos em contato!

E-mail: leonardo99.armelin@gmail.com
